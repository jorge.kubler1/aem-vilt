import { MapTo } from '@adobe/aem-angular-editable-components';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public backgroundColor:string;

  public logoURL:string;
  public logoPath:string;
  public logoLink:string;

  public menuObject:[];

  public buttonLink:string;
  public buttonText:string;

  constructor() { }

  ngOnInit(): void {
  }

}

const HeaderEditConfig = {
  emptyLabel: 'HEADER',
  isEmpty(componentData) { return !componentData || !componentData.description; }
};

MapTo('aem-vilt/components/header')
(HeaderComponent, HeaderEditConfig)

